package uz.pdp.appsecurityfirst.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// Nurkulov Nodirbek 4/7/2022  9:31 AM
@RestController
@RequestMapping("/login")
@RequiredArgsConstructor
public class LoginController {

    @GetMapping
    public String getLoginPage(){
        return "login";
    }
}
