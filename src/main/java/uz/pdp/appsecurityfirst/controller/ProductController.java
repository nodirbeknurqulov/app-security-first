package uz.pdp.appsecurityfirst.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appsecurityfirst.entity.Product;
import uz.pdp.appsecurityfirst.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

// Nurkulov Nodirbek 3/24/2022  9:45 PM
@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {

    /**
     * bu yerda aslida service ochib crud ni qilish kerak edi.
     * Lekin vaqt ketkizmaslik maqsad spring security ni tushunib olish uchun
     * unday qilinmadi
     */
    private final ProductRepository productRepository;

    //MANAGER, DIRECTOR
    //preAuthorize bu ROLE BASED authorization
    @PreAuthorize(value = "hasAuthority('READ_ALL_PRODUCT')")
    @GetMapping
    public HttpEntity<?> getAllProducts() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ResponseEntity.ok(productRepository.findAll());
    }

    //DIRECTOR
    @PreAuthorize(value = "hasAuthority('ADD_PRODUCT')")
    @PostMapping
    public HttpEntity<?> addProduct(@RequestBody Product product) {
        return ResponseEntity.ok(productRepository.save(product));
    }

    // DIRECTOR
    @PreAuthorize(value = "hasAuthority('EDIT_PRODUCT')")
    @PutMapping("/{id}")
    public HttpEntity<?> editProduct(@PathVariable Integer id, @RequestBody Product product) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        if (optionalProduct.isPresent()) {
            Product editingProduct = optionalProduct.get();
            editingProduct.setName(product.getName());
            productRepository.save(editingProduct);
            return ResponseEntity.ok(editingProduct);
        }
        return ResponseEntity.notFound().build();
    }

    //DIRECTOR
//    @PreAuthorize(value = "hasAuthority('DELETE_PRODUCT')")
    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteProduct(@PathVariable Integer id) {
        productRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    //MANAGER, DIRECTOR. USER
    @PreAuthorize(value = "hasAuthority('READ_ONE_PRODUCT')")
    @GetMapping("/{id}")
    public HttpEntity<?> getProductById(@PathVariable Integer id) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        return ResponseEntity.status(optionalProduct.isPresent() ? 200 : 404).body(optionalProduct.get());
    }
}
